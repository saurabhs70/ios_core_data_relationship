//
//  Student+CoreDataProperties.swift
//  dv
//
//  Created by saurabh-pc on 26/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import Foundation
import CoreData


extension Student {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Student> {
        return NSFetchRequest<Student>(entityName: "Student");
    }

    @NSManaged public var address: String?
    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var studid: Int16
    @NSManaged public var studentclass: Studentclass?
    @NSManaged public var subjects: NSSet?

}

// MARK: Generated accessors for subjects
extension Student {

    @objc(addSubjectsObject:)
    @NSManaged public func addToSubjects(_ value: Subject)

    @objc(removeSubjectsObject:)
    @NSManaged public func removeFromSubjects(_ value: Subject)

    @objc(addSubjects:)
    @NSManaged public func addToSubjects(_ values: NSSet)

    @objc(removeSubjects:)
    @NSManaged public func removeFromSubjects(_ values: NSSet)

}
