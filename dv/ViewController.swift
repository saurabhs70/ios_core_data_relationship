//
//  ViewController.swift
//  dv
//
//  Created by saurabh-pc on 21/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var listsarr=[Studentclass]()
    var arrlist = [Student]()
    
    var clickedclass:Int16?
    
    @IBOutlet weak var listtbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        listsarr = Dbmanager.sharedInstance.getallclass()! as! [Studentclass]
        print(listsarr)
        listtbl.reloadData()
       // var student:Student=lists[0] as! Student;
         //   print("\(lists[0]) \n \(student.name)")
        
//        let animalOne = Student()
//       animalOne.name="pp"
//        animalOne.address="address"
//        
//        let animaltwo = Student()
//        animaltwo.name="pp"
//        animaltwo.address="address"
//        
//        let myList: Array<Any> = [animalOne, animaltwo];
//        
//        let ani:Student = myList[0] as! Student
        


    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listsarr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCell", for: indexPath) as UITableViewCell

        let  obj:Studentclass=self.listsarr[indexPath.row]
        // set the text from the data model
        cell.textLabel?.text = String(format:"\(obj.name!) (\(obj.student!.count)) student  ")//obj.name//self.listsarr[indexPath.row]
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
         let  obj:Studentclass=self.listsarr[indexPath.row]
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController:AddStudentViewController = storyBoard.instantiateViewController(withIdentifier: "addstudent") as! AddStudentViewController
//        nextViewController.strungid=1
      //  navigationController?.pushViewController(nextViewController, animated: true)
       // self.presentViewController(nextViewController, animated:true, completion:nil)
        
        
        //prepare(for: <#T##UIStoryboardSegue#>, sender: nil)
        clickedclass=obj.class_id
        let nsarr = obj.student?.allObjects as! NSArray //NSSet() //NSSet
        //var arr = set.allObjects //Swift Array
        //let nsarr = set.allObjects as NSArray  //NSArray
       // let nsarr:NSArray = obj.student?.allObjects as! NSArray
        arrlist=nsarr as! [Student]
        print(arrlist)
        performSegue(withIdentifier: "liststudentsid", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="addstudentid" {
            let obj = segue.destination as! AddStudentViewController
            obj.strungid=clickedclass//sender as! Int?
        }
        else if  segue.identifier=="liststudentsid"
        {
            let obj = segue.destination as! listStudentsViewController
                        obj.listsarr=arrlist
            obj.objClassId=clickedclass
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        //        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
        ////            let alert = UIAlertController(title: "", message: "Edit list item", preferredStyle: .alert)
        ////            alert.addTextField(configurationHandler: { (textField) in
        ////                textField.text = self.lists[indexPath.row] as? String
        ////            })
        ////            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (updateAction) in
        ////                self.lists[indexPath.row] = alert.textFields!.first!.text! as AnyObject
        ////                self.tbllist.reloadRows(at: [indexPath], with: .fade)
        ////            }))
        ////            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ////            self.present(alert, animated: false)
        //        })
        //
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
            
                             let  obj:Studentclass=self.listsarr[indexPath.row]
            
                            let checkdelete=Dbmanager.sharedInstance.deleteFromModel(objstu: obj)
                                if checkdelete
                            {
                                self.listsarr.remove(at: indexPath.row)
                                self.listtbl.reloadData()
                                let ffafter=Dbmanager.sharedInstance.getall(stringforentity: "Student")
                                print(ffafter?.count ?? 555)
                            }
            
                      })
        
        // return [deleteAction, editAction]
        return [deleteAction]
    }

    
}

