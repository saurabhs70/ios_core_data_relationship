//
//  Dbmanager.swift
//  dv
//
//  Created by saurabh-pc on 21/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit
import CoreData
class Dbmanager: NSObject {

   static let sharedInstance = Dbmanager()
    
    func saveTomodel(stringname:String,stringmobile:String, stringadd:String) -> Bool {
        
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context=appdel.persistentContainer.viewContext
        let newuser=NSEntityDescription.insertNewObject(forEntityName: "Student", into: context)
        
        newuser.setValue(stringname, forKey: "name")
        newuser.setValue(stringadd, forKey: "address")
        newuser.setValue(stringmobile, forKey: "email")
        newuser.setValue(pkid(), forKey: "studid")

        
        do
        {
            try context.save();
            return true;
            
        }
        catch
        {
            return false;
        }
    }
    //MARK:- Add class
    func addClass(stringclass:String) -> Bool {
        if !stringclass.isEmpty {
            
        
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let newclass = NSEntityDescription.insertNewObject(forEntityName: "Studentclass", into: context)
        newclass.setValue(stringclass, forKey: "name")
        newclass.setValue(pkid(), forKey: "class_id")
        do
        {
            try context.save()
            return true
        }
        catch
        {
            return false
        }
        }
        else
        {
            return false
        }
    }
    //MARK:- Delete class
    func deleteFromModel(objstu:Studentclass) -> Bool {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context=appdel.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Studentclass")
        fetchRequest.returnsObjectsAsFaults=false
        fetchRequest.predicate = NSPredicate(format: "class_id = %d", objstu.class_id)
        do
        {
            let counfod = try context.fetch(fetchRequest)
            if counfod.count>0 {
                let objstu:Studentclass=counfod[0] as! Studentclass
                context.delete(objstu)
                do
                {
                    try context.save()
                    return true
                }
                catch
                {
                    return false
                }
            
            }
            
        }
        catch
        {
            return true

        }

        return true
    }
    //MARK:- Add student into class
    func addStudents(stringname:String,stringemail:String, stringadd:String,classid:Int16) ->Bool {
      //  if stringcl {
            
            
            let appdel = UIApplication.shared.delegate as! AppDelegate
            let context = appdel.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Studentclass")
        fetchRequest.returnsObjectsAsFaults=false
        fetchRequest.predicate = NSPredicate(format: "class_id = %d", classid)

            do
            {
                         //  let objst:Studentclass =
                let test = try context.fetch(fetchRequest)
                if test.count==1 {
                    let objstu:Studentclass=test[0] as! Studentclass
                    print("found")
                    let addnewstudent = NSEntityDescription.insertNewObject(forEntityName: "Student", into: context)
                    addnewstudent.setValue(pkid(), forKey: "studid")
                    addnewstudent.setValue(stringadd, forKey: "address")
                    addnewstudent.setValue(stringemail, forKey: "email")
                    addnewstudent.setValue(stringname, forKey: "name")
                   // objstu.setValue(NSSet(object: addnewstudent), forKey: "student")
                   // objstu.studentclass=addnewstudent
                    let studetntslist = objstu.mutableSetValue(forKey: "student")
                    studetntslist.add(addnewstudent)
                    do
                    {
                        try context.save()
                              print("saved")
                            //print("erroir")
                        return true
                    }
                   catch
                   {
                    print("not saved")
                    return false
                    }
                    
               // context.delete(test[0] as! NSManagedObject)
                    //context.save()
                }
                else
                {
                    print("no found")
                }
               // return true
            }
            catch
            {
                print("no found")
                return false

                //return false
            }
//        }
//        else
//        {
//            return false
//        }
        return false
    }
    //MARK:- Get all class
    func getallclass()-> Array<Any>? {
        
        let arra_class:Array<Any>?
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context=appdel.persistentContainer.viewContext
        let fetchreq = NSFetchRequest<NSFetchRequestResult>(entityName:"Studentclass")
        fetchreq.returnsObjectsAsFaults=false
        do
        {
            arra_class=try context.fetch(fetchreq)
            return arra_class
        }
        catch
        {
            return nil
        }
        
        
    }
    //MARK:- Get all entity
    func getall(stringforentity:String)->Array<Any>?  {
        let array_users:Array<Any>?;
        let appdel=UIApplication.shared.delegate as! AppDelegate;
        let context=appdel.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: stringforentity)
         fetchRequest.returnsObjectsAsFaults=false
        //fetchRequest.predicate=NSPredicate(format: "user= %@","")
        do {
array_users = try context.fetch(fetchRequest)
            return array_users!;
            //print("\(array_users)")
        }
        catch
        {
            return nil;
        }
        //var fetchreq=NSFetchRequest(entityname:"Vehicle")
        
    }
    
//MARK:- LOAD subject
    func loadsubject(lists:Array<Any>)  {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        for  dict in lists {
            let context=delegate.persistentContainer.viewContext
            let nam:NSDictionary=dict as! NSDictionary
            let subid:Int64 = Int64((nam.object(forKey: "subid")! as! NSString).intValue)
            let subname = nam.object(forKey: "subname")!
            
            print("item  \(nam.object(forKey: "subname")!)" )
        
        let addsubject = NSEntityDescription.insertNewObject(forEntityName: "Subject", into: context)
        addsubject.setValue(subid, forKey: "subid")
        addsubject.setValue(subname, forKey: "name")
        do
        {
            try context.save()
            print("")
        }
        catch
        {
            print("")
            
        }
        }
        
        let lists = getall(stringforentity: "Subject") as! [Subject]
        print("\(lists)")
        
    }
        func pkid() -> Int16 {
        let randomNum:UInt16 = UInt16(arc4random_uniform(10000)) // range is 0 to 99
        
        // convert the UInt32 to some other  types
        
       // let randomTime:TimeInterval = TimeInterval(randomNum)
        
        let someInt16:Int = Int(Int16(randomNum))
        return Int16(someInt16)
    }
    
    func createObject(stringName:String,stringId:Int16) -> Subject {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        
        let addsubject = NSEntityDescription.insertNewObject(forEntityName: "Subject", into: context)
        addsubject.setValue(stringId, forKey: "subid")
        addsubject.setValue(stringName, forKey: "name")
        return addsubject as! Subject;

    }
    //MARK:- update subject to student
    func updateSubject(studentId:Int16,studentsubj:[Subject])->Bool {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let fetreq = NSFetchRequest<NSFetchRequestResult>(entityName:"Student")
        fetreq.returnsObjectsAsFaults=false
        fetreq.predicate = NSPredicate(format: "studid = %d",studentId)
        let set = Set(studentsubj )
        do
        {
            let counfod = try context.fetch(fetreq)
            if counfod.count>0 {
                let objstu:Student=counfod[0] as! Student
                objstu.subjects=set as NSSet?
                //context.save()
                do
                {
                    try context.save()
                    return true
                }
                catch
                {
                    //return false
                }
                
            }
            
        }
        catch
        {
            return true
            
        }

return false
    }
    
}
