//
//  listStudentsViewController.swift
//  dv
//
//  Created by saurabh-pc on 26/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class listStudentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   // var strungid:[Student]?
    
    var listsarr:[Student]?
    var objClassId:Int16?
    var  objstudent:Student?
    
    
    @IBAction func btnAddStudenClicked(_ sender: Any) {
        performSegue(withIdentifier: "addstudent", sender: self)
    }
    @IBOutlet weak var tblStudents: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
      //  listsarr = Dbmanager.sharedInstance.getallclass()! as! [Student]
        //print(listsarr ?? <#default value#>)
        tblStudents.reloadData()
        // var student:Student=lists[0] as! Student;
        //   print("\(lists[0]) \n \(student.name)")
        
        //        let animalOne = Student()
        //       animalOne.name="pp"
        //        animalOne.address="address"
        //
        //        let animaltwo = Student()
        //        animaltwo.name="pp"
        //        animaltwo.address="address"
        //
        //        let myList: Array<Any> = [animalOne, animaltwo];
        //
        //        let ani:Student = myList[0] as! Student
        
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listsarr!.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "listcell", for: indexPath) as UITableViewCell 
        
        let  obj:Student=self.listsarr![indexPath.row]
        // set the text from the data model
        cell.textLabel?.text = String(format:"\(obj.name!) with subjects (\(obj.subjects!.count))")//obj.name//self.listsarr[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let  obj:Student=self.listsarr![indexPath.row]
       objstudent=obj
        performSegue(withIdentifier: "listofsubjects", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="listofsubjects" {
        let obj = segue.destination as! listSubjectViewController
        obj.objStudentID=objstudent//sender as! Int?
        }
        else if  segue.identifier=="liststudentsid"
        {
            //let obj = segue.destination as! listStudentsViewController
           // obj.listsarr=arrlist
            
        }
        else if segue.identifier=="addstudent" {
            let obj = segue.destination as! AddStudentViewController
            obj.strungid=objClassId//sender as! Int?
        }

    }

}
