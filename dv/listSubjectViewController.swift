//
//  listSubjectViewController.swift
//  dv
//
//  Created by saurabh-pc on 28/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import UIKit

class listSubjectViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var lists = [Subject]();
    var objStudentID:Student?
    var arrcheck = [Int16]()
    
    
    @IBOutlet weak var listtbl: UITableView!
    
    @IBAction func saveClicked(_ sender: Any) {
       // arrcheck=[1,2,3,7]
        var arrsubj = [Subject]()
        for objsub  in arrcheck
        {
            for objs in lists
            {
                if objs.subid==objsub {
                    arrsubj.append(objs)

                }
            }
           // let obj=Int(objsub)
            
           // let objinSub = lists[obj]
           // arrsubj.append(objinSub)
        }
    let up=Dbmanager.sharedInstance.updateSubject(studentId: (objStudentID?.studid)!, studentsubj: arrsubj)
        if up {
            
        }
        print(arrsubj)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        for objstu in (objStudentID?.subjects)!
        {
            let val=(objstu as! Subject).subid 
            arrcheck.append(val)
        }
//arrcheck=
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        lists = Dbmanager.sharedInstance.getall(stringforentity: "Subject")! as! [Subject]
        print(objStudentID?.subjects ?? "")
 listtbl.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "subjectcell", for: indexPath) as UITableViewCell
        let obj:Subject = lists[indexPath.row]
        if arrcheck.contains(obj.subid) {
            cell.accessoryType = .checkmark

        }
        else
        {
        cell.accessoryType = .none
        }
        //let  obj:Studentclass=self.listsarr[indexPath.row]
        // set the text from the data model
        cell.textLabel?.text = String(format:"\(obj.name! )")//obj.name//self.listsarr[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("")
        let obj:Subject = lists[indexPath.row]
        if arrcheck.contains(obj.subid) {
          arrcheck = arrcheck.filter { $0 != obj.subid }
        }
        else
        {
            arrcheck.append(obj.subid)
        }
        self.listtbl.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
