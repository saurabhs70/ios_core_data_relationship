//
//  Subject+CoreDataProperties.swift
//  dv
//
//  Created by saurabh-pc on 26/08/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

import Foundation
import CoreData


extension Subject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Subject> {
        return NSFetchRequest<Subject>(entityName: "Subject");
    }

    @NSManaged public var subid: Int16
    @NSManaged public var name: String?
    @NSManaged public var student: NSSet?

}

// MARK: Generated accessors for student
extension Subject {

    @objc(addStudentObject:)
    @NSManaged public func addToStudent(_ value: Student)

    @objc(removeStudentObject:)
    @NSManaged public func removeFromStudent(_ value: Student)

    @objc(addStudent:)
    @NSManaged public func addToStudent(_ values: NSSet)

    @objc(removeStudent:)
    @NSManaged public func removeFromStudent(_ values: NSSet)

}
